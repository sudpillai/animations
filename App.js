import React from 'react';
import { StyleSheet, Text,ScrollView,Button, Animated,TouchableOpacity,View,TextInput
  ,  SafeAreaView, TouchableHighlight, Dimensions, Image } from 'react-native';


var isHidden = true;
var isHiddenSecondTransition = true;
var screen_height = Dimensions.get('window').height;
var screen_width = Dimensions.get('window').width;
var initialText = 'See more cool stuff for you!';
var finalText = 'we know you love Mexican!';
var isAnimationDone  = false;

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
       //This is the initial position of the subview
      buttonText: "Show Subview",
      bounceValueMainView: new Animated.Value(0),
      bounceValue: new Animated.Value(0), 
      bounceValueSecondView: new Animated.Value(0),
    };
  }

  _toggleSubview() {    
    this.setState({
      buttonText: !isHidden ? "Show Subview" : "Hide Subview"
    });

    var toValue = -screen_height/2;
    var toValueMainView = -screen_height/2;
    if(!isHidden) {
      toValue = screen_height;
      toValueMainView = 0;
    }

    //This will animate the transalteY of the subview between 0 & 100 depending on its current state
    //100 comes from the style below, which is the height of the subview.
    Animated.spring(
      this.state.bounceValue,
      {
        toValue: toValue,
        velocity: 3,
        tension: 2,
        friction: 8,
      }
    ).start();

    Animated.spring(
      this.state.bounceValueMainView,
      {
        toValue: toValueMainView,
        velocity: 3,
        tension: 2,
        friction: 8,
      }
    ).start();

    
    isAnimationDone  = !isAnimationDone ;
    isHidden = !isHidden;
  }

  SecondSubview() {    

    var toValue = -(Dimensions.get('window').height);
    var toValueMainView = -(Dimensions.get('window').height);
    var toValueSecondSubView = -(screen_height/2 + Dimensions.get('window').height);
    if(!isHiddenSecondTransition) {
      toValue = screen_height/2;
      toValueMainView = 0;
      toValueSecondSubView = Dimensions.get('window').height;
    }

    //This will animate the transalteY of the subview between 0 & 100 depending on its current state
    //100 comes from the style below, which is the height of the subview.
    Animated.spring(
      this.state.bounceValue,
      {
        toValue: toValue,
        velocity: 3,
        tension: 2,
        friction: 8,
      }
    ).start();

    Animated.spring(
      this.state.bounceValueMainView,
      {
        toValue: toValueMainView,
        velocity: 3,
        tension: 2,
        friction: 8,
      }
    ).start();

    Animated.spring(
      this.state.bounceValueSecondView,
      {
        toValue: toValueSecondSubView,
        velocity: 3,
        tension: 2,
        friction: 8,
      }
    ).start();

    

    isHiddenSecondTransition = !isHiddenSecondTransition;
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Animated.View style={[styles.mainView,
              {transform: [{translateY: this.state.bounceValueMainView}]}]}
              >
                <View style={{flex: 8, justifyContent: 'center', alignItems: 'center'}}>
                  <Text>Image here</Text>
                </View>
                <View style={{flex: 1, alignItems: 'flex-start', width: '85%'}}>
                <Text style={{fontWeight: 'bold', fontSize: 20}}>now let's save you some money</Text>
                    <Text style={{fontWeight: '100', fontSize: 20}}>...our best pack for UAE just for you!</Text>
                </View>
                <View style={styles.offerDetailsStyle}>
                  <View style={{flex: 3,justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', fontSize: 20}}>2999</Text>
                  </View>
                  <View style={{flex: 2,  justifyContent: 'center', alignItems: 'flex-start'}}>
                    <Text style={{fontWeight: '100'}}>Data</Text>
                    <Text style={{fontWeight: 'bold'}}>1 GB</Text>
                  </View>
                  <View style={{flex: 3,justifyContent: 'center', alignItems: 'flex-start'}}>
                  <Text style={{fontWeight: '100'}}>Data</Text>
                    <Text style={{fontWeight: 'bold'}}>Unlimited</Text>
                  </View>
                  <View style={{flex: 3,justifyContent: 'center', alignItems: 'flex-start'}}>
                  <Text style={{fontWeight: '100'}}>Days</Text>
                    <Text style={{fontWeight: 'bold'}}>7</Text>
                  </View>
                </View>
                <View style={{flex: 1, width: '100%', alignItems: 'center' }}>
                  <TouchableOpacity style={styles.activateNowStyle}>
                    <Text style={styles.activateText}>Activate Now!</Text>
                  </TouchableOpacity>
                </View>
                <View style={{flex: 1, alignItems: 'flex-start', width: '100%'}}>
                <TouchableHighlight style={styles.button} onPress={()=> {this._toggleSubview()}}>
            <Text style={styles.buttonText} >{isAnimationDone ? finalText : initialText}</Text>
          </TouchableHighlight>
                </View>
          
          </Animated.View>
          <Animated.View
            style={[styles.subView,
              {transform: [{translateY: this.state.bounceValue}]}]}
          >
           <View style={{flex: 1,justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
              <View style={{flex:1}}>
                <Image source={require('./assets/Bitmap.png')} resizeMode='contain' style={{width: undefined}}></Image>
              </View>
              <View style={{flex:2}}>
                <Text style={{paddingLeft:20, fontSize:18}}>Ahlan, First Class Lounge,</Text>
                <TouchableOpacity style={{borderColor: 'blue',marginLeft:20, borderWidth: 2, width: 150}}>
                <Text style={{ fontSize:18,color: 'blue', textAlign:'center'}}>View on Mzp</Text>
                </TouchableOpacity>
              </View>
           </View>
           <View style={{flex: 1, backgroundColor: "rgba(0,0,0,0.6)",justifyContent: 'flex-end'}}>
             {/* <Image source={require('./assets/pic.png')} style={{width: 50, height: 50}}></Image> */}
                {/* <View style={{width: 50, height: 50, zIndex: 4, justifyContent: 'flex-end'}}>
                  <Text>reger</Text>
                </View> */}
              <View style={{backgroundColor: 'white', height: 80, width: 200, marginLeft: 70, marginBottom: 50, borderRadius: 10}}>
                <View style={{flex:1 }}>
                  <Text style={{fontSize: 20, marginLeft: 10, fontWeight: '100'}}>Are you here for</Text>
                </View>
                <View style={{flex:1, flexDirection: 'row', alignItems: 'center' }}>
                  <Button title='pleasure' color='blue' borderColor='blue' onPress={() => this.SecondSubview()}  ></Button>
                  <Button title='business' color='blue'></Button>
                </View>
              </View>
           </View>
          </Animated.View>
          <Animated.View
            style={[styles.secondSubView,
              {transform: [{translateY: this.state.bounceValueSecondView}]}]}
          >
            <View style={{flex:2,width:'100%', flexDirection: 'row'}}>
              <View >
                <Image source={require('./assets/face.png')}  style={{width: 40, height: 40, marginTop: 70, marginLeft: 20}}></Image>
              </View>
            <View style={{justifyContent: 'center',backgroundColor: 'white', height: 80, width: 200, marginLeft: 20, marginTop: 40, borderRadius: 10,shadowColor: '#000000',
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowRadius: 2,
  shadowOpacity: 0.6,}}>
                <View style={{flex:1 }}>
                  <Text style={{fontSize: 20, marginLeft: 10, fontWeight: '100'}}>Are you here for</Text>
                </View>
                <View style={{flex:1, flexDirection: 'row', alignItems: 'center' }}>
                  <Button title='pleasure' color='blue' borderColor='blue' onPress={() => this.SecondSubview()}  ></Button>
                  <Button title='business' color='blue'></Button>
                </View>
              </View>
            </View>
            <View style={{flex:1, width:'100%', flexDirection: 'row'}}>
            <View style={{justifyContent: 'center',backgroundColor: 'white', height: 40, width: 100,marginTop: 10, borderRadius: 10,marginLeft: 220,shadowColor: '#000000',
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowRadius: 2,
  shadowOpacity: 0.6,}}>
                <View style={{flex:1 }}>
                  <Text style={{fontSize: 20,fontWeight: '100', marginLeft: 10}}>pleasure</Text>
                </View>
              </View>
            <View >
                <Image source={require('./assets/face.png')}  style={{width: 40, height: 40, marginRight: 20, marginTop: 10}}></Image>
              </View>
            
            </View>
            <View style={{flex:2,width:'100%', flexDirection: 'row'}}>
               <View >
                <Image source={require('./assets/face.png')}  style={{width: 40, height: 40, marginTop: 70, marginLeft: 20}}></Image>
              </View>
               <View style={{justifyContent: 'center',backgroundColor: 'white', height: 80, width: 250, marginLeft: 20, marginTop: 40, borderRadius: 10,flexDirection: 'column',shadowColor: '#000000',
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowRadius: 2,
  shadowOpacity: 0.6,}}>
                <View style={{flexWrap:'wrap'}}>
                  <Text style={{fontSize: 20, marginLeft: 10, fontWeight: '100', flexWrap: 'wrap'}}>Awesome! You've picked the perfect season for it.</Text>
                </View>
                
              </View>
            </View>
            <View style={{flex:2, width:'100%'}}>
            <View style={{justifyContent: 'center',backgroundColor: 'white', height: 80, width: 250, marginLeft: 80, marginTop: 40, borderRadius: 10,flexDirection: 'column',shadowColor: '#000000',
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowRadius: 2,
  shadowOpacity: 0.6,}}>
                <View style={{flexWrap:'wrap'}}>
                  <Text style={{fontSize: 20, marginLeft: 10, fontWeight: '100', flexWrap: 'wrap'}}>and.. her is stuff you and fam are absolutely going to love.</Text>
                </View>
                
              </View>
            </View>
            <View style={{flex:4, width:'100%'}}>
              <Image source={require('./assets/face.png')} resizeMode='contain' style={{width: '100%', height: '80%', marginTop: 20, marginBottom: 20}}></Image>
            </View>
            <View style={{ width: '50%',flex:1 }}>
            <TouchableOpacity style={{backgroundColor: '#0088bf',marginBottom: 30, borderRadius: 5, height: 40, justifyContent: 'center'}}>
              <Text style={{justifyContent: 'center',color: 'white', textAlign: 'center', fontSize: 20}}>book now</Text>
            </TouchableOpacity>
            </View>
            <View style={{width: '90%', height: 40, marginBottom: 30, flex:1}}>
              {/* <Image source={require('./assets/Dictation.png')} style={{zIndex: 2}}></Image> */}
            <TextInput style={{backgroundColor: 'grey', textAlign: 'left', opacity: 0.2,borderRadius: 5, paddingHorizontal: 30, height: '70%'}} placeholder='Type here' placeholderTextColor='black'></TextInput>
            </View>
          </Animated.View>
      </ScrollView>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
   
  },
  button: {
    padding: 8,
  },
  buttonText: {
    fontSize: 17,
    color: "black",
    marginLeft: 25,
    fontWeight: isAnimationDone ? 'bold' : '100'
  },
  subView: {
    height: screen_height/2,
    
  },
  mainView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  secondSubView: {
    flex:1,
    backgroundColor: 'white',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  activateNowStyle: {
   width: '85%',
   marginTop:10,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'#0088bf',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff'
  },
  activateText:{
    color:'#fff',
    textAlign:'center',
    paddingLeft : 10,
    paddingRight : 10,
    fontWeight: 'bold'
},
offerDetailsStyle: {
  flex: 1,
  marginTop: 20,
  marginBottom: 20,
  backgroundColor: 'white', 
  borderColor: 'white',
  borderRadius: 6,
  width: '85%',
  height: '90%',
  shadowColor: '#000000',
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowRadius: 2,
  shadowOpacity: 0.6,
  flexDirection: 'row'
}
});